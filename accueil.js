document.addEventListener("DOMContentLoaded",function(){

    var ClassInstanceName = "slide"
    if (typeof(ClassInstanceName) === "object") {
        console.log("cool")
     }


    var controller = new ScrollMagic.Controller();
    var slides = document.querySelectorAll(".slide");
    slides.forEach(function( index ) {
        new ScrollMagic.Scene({triggerElement: '#'+ index.id, triggerHook: 'onLeave'})
        .setClassToggle('#'+ index.id, "scroll") // add class toggle
        //.addIndicators({name: "fixed => scroll"}) // add indicators (requires plugin)
        .addTo(controller);
        //assombrir('#'+index.id);
    });
    
    function assombrir(selecteur){
        var tween = TweenMax.to({}, 2, {
            onUpdateParams:["{self}"],
            onUpdate:function(tl){
                var tlp = (tl.progress()*100)>>0;
                TweenMax.set(selecteur,{
                    '-webkit-filter':'brightness(' + tlp + '%' + ')',
                    'filter':'brightness(' + tlp + '%' + ')'
                });
            }
        });
        var hauteur = document.querySelector(selecteur).offsetHeight / 1; // mettre /2 pour que l'effet soit plus rapide
        var scene = new ScrollMagic.Scene({triggerElement: selecteur, duration: hauteur, triggerHook: 'onEnter'})
            .setTween(tween)
            //.addIndicators({name: "tween css class"}) // add indicators (requires plugin)
            .addTo(controller);
    }
});