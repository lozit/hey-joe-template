document.addEventListener("DOMContentLoaded",function(){
  
  // Menu
  if(!!document.querySelector(".switch")) {
    function expand() {
      var menu = document.querySelector("body");
      menu.classList.toggle('ouvert');
    }
    var el = document.querySelector(".switch");
    el.addEventListener("click", expand, false);
  } 

  // home texte
  if(!!document.querySelector(".edito")) {
    var controller = new ScrollMagic.Controller();
    new ScrollMagic.Scene({triggerElement: '.contenu', triggerHook: 'onLeave', duration: "50%"})
        .setClassToggle('body', "appear") // add class toggle
        //.addIndicators({name: "apparition"}) // add indicators (requires plugin)
        .addTo(controller);
        //assombrir('#'+index.id);

    new ScrollMagic.Scene({triggerElement: '.contenu', triggerHook: 'onLeave', offset: "50%"})
        .setClassToggle('body', "petit") // add class toggle
        .addIndicators({name: "reportage"}) // add indicators (requires plugin)
        .addTo(controller);
        //assombrir('#'+index.id);
 }

  // rdm effect
  if(!!document.querySelector(".slide")) {
      var controller = new ScrollMagic.Controller();
      var slides = document.querySelectorAll(".slide");
      slides.forEach(function( index ) {
          new ScrollMagic.Scene({triggerElement: '#'+ index.id, triggerHook: 'onLeave'})
          .setClassToggle('#'+ index.id, "scroll") // add class toggle
          //.addIndicators({name: "fixed => scroll"}) // add indicators (requires plugin)
          .addTo(controller);
          //assombrir('#'+index.id);
      });
      
      // need tweenmax but very slow
      function assombrir(selecteur){
          var tween = TweenMax.to({}, 2, {
              onUpdateParams:["{self}"],
              onUpdate:function(tl){
                  var tlp = (tl.progress()*100)>>0;
                  TweenMax.set(selecteur,{
                      '-webkit-filter':'brightness(' + tlp + '%' + ')',
                      'filter':'brightness(' + tlp + '%' + ')'
                  });
              }
          });
          var hauteur = document.querySelector(selecteur).offsetHeight / 1; // mettre /2 pour que l'effet soit plus rapide
          var scene = new ScrollMagic.Scene({triggerElement: selecteur, duration: hauteur, triggerHook: 'onEnter'})
              .setTween(tween)
              //.addIndicators({name: "tween css class"}) // add indicators (requires plugin)
              .addTo(controller);
      }
   }
});