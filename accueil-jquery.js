$(function() {
    var controller = new ScrollMagic.Controller();
    
    $( ".slide" ).each(function( index ) {
        new ScrollMagic.Scene({triggerElement: '#'+$(this).attr('id'), triggerHook: 'onLeave'})
        .setClassToggle('#'+$(this).attr('id'), "scroll") // add class toggle
        //.addIndicators({name: "fixed => scroll"}) // add indicators (requires plugin)
        .addTo(controller);
        //assombrir('#'+$(this).attr('id'));
    });
    
    function assombrir(selecteur){
        var tween = TweenMax.to({}, 2, {
            onUpdateParams:["{self}"],
            onUpdate:function(tl){
                var tlp = (tl.progress()*100)>>0;
                TweenMax.set(selecteur,{
                    '-webkit-filter':'brightness(' + tlp + '%' + ')',
                    'filter':'brightness(' + tlp + '%' + ')'
                });
            }
        });
        var hauteur = $(selecteur).outerHeight()/1; // mettre /2 pour que l'effet soit plus rapide
        var scene = new ScrollMagic.Scene({triggerElement: selecteur, duration: hauteur, triggerHook: 'onEnter'})
            .setTween(tween)
            //.addIndicators({name: "tween css class"}) // add indicators (requires plugin)
            .addTo(controller);
    }
});